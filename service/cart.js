import axios from "axios";

const getAllProducts = async () => {
    return axios.get('https://fakestoreapi.com/carts')
    .then(function (response) {
        return response.data;
    })
    .catch (function (error) {
        console.log(error);
    })
}
export {getAllProducts}