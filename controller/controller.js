import { getAllProducts } from "../service/cart.js";

const fetchAllProducts = async () => {
    try {
        const allProducts = await getAllProducts();
        return allProducts; 
    } catch (error) {
        console.log(error);
    }
}
const getOneCart =(Argument,userId) => {
    return Argument.filter(allProducts => allProducts.userId === userId);
}
export {fetchAllProducts,getOneCart};